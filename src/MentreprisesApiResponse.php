<?php

namespace Mentreprises\Api;

class MentreprisesApiResponse
{
    private $statusCode;
    private $rawContent;
    private $parsedContent;

    public function __construct($statusCode, $rawResponse)
    {
        $this->statusCode = $statusCode;
        $this->rawContent = $rawResponse;
        $this->parsedContent = @json_decode($rawResponse, true);
    }

    public function getStatusCode()
    {
    	return $this->statusCode;
    }

    public function isSuccess()
    {
    	return $this->get('success');
    }

    public function getRawContent()
    {
        return $this->rawContent;
    }

    public function getContent()
    {
        return $this->parsedContent;
    }

    public function getData()
    {
    	return $this->get('data');
    }

    public function get($paramName)
    {
    	return isset($this->parsedContent[$paramName]) ? $this->parsedContent[$paramName] : null;
    }
}