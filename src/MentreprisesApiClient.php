<?php

namespace Mentreprises\Api;

class MentreprisesApiClient
{
    const SERVICE_URLS = [
        'default' => 'https://meilleures-entreprises.com',
        'happy' => 'https://happysurveys.org',
    ];

    private $accessToken;
    private $baseUrl;
    private $recordCalls;
    private $recordedCalls;

    public static function create($service = 'default', $accessToken = null)
    {
        if (!is_string($service) || !array_key_exists($service, self::SERVICE_URLS)) {
            throw new \Exception('Please provide a valid service (' . implode(', ', array_keys(self::SERVICE_URLS)) . ')');
        }
        return new self(self::SERVICE_URLS[$service], $accessToken);
    }
    
    public function __construct($baseUrl, $accessToken = null, $recordCalls = false)
    {
        if ((!is_string($baseUrl) || !filter_var($baseUrl, FILTER_VALIDATE_URL))) {
            throw new \Exception('Please provide a valid URL as baseUrl');
        }

        $this->accessToken = $accessToken;
        $this->baseUrl = $baseUrl;
        $this->recordCalls = $recordCalls;
        $this->recordedCalls = [];
    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    public function call($path, $method = 'get', $parameters = [])
    {
        $ch = curl_init();
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Authorization: Bearer ' . $this->accessToken]);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, strtoupper($method));

        $url = $this->baseUrl . $path;

        if ((strtolower($method) === 'get') && $parameters) {
            $url .= (strpos($url, '?') ? '&' : '?') . http_build_query($parameters);
        } else {
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($parameters));
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        
        $microtimeStart = $this->recordCalls ? microtime(true) : null;
        
        $content = curl_exec($ch);
        $info = curl_getinfo($ch);
        
        if ($this->recordCalls) {
            $this->recordedCalls[] = [
                'url' => $url,
                'method' => $method,
                'took' => round(microtime(true) - $microtimeStart, 3) . 's',
                'responseCode' => $info['http_code'],
            ];
        }
        
        curl_close($ch);
        
        return new MentreprisesApiResponse($info['http_code'], $content);
    }
    
    public function getRecordedCalls()
    {
        return $this->recordedCalls;
    }

}
