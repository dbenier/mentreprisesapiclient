<?php 

use Mentreprises\Api\MentreprisesApiClient;
use Mentreprises\Api\MentreprisesApiResponse;

class MentreprisesApiClientTest extends \PHPUnit_Framework_TestCase
{
 	public function testWithoutToken()
 	{
 		$client = MentreprisesApiClient::create();

		/* make api call */
		$response = $client->call('/api/company/');

		$this->assertInstanceOf(MentreprisesApiResponse::class, $response);
		$this->assertFalse($response->isSuccess());
 	}

 	public function testWithInvalidToken()
 	{
 		$client = MentreprisesApiClient::create('default', 'WRONG TOKEN');

		/* make api call */
		$response = $client->call('/api/company/');

		$this->assertInstanceOf(MentreprisesApiResponse::class, $response);
		$this->assertFalse($response->isSuccess());
 	}
}
